'use strict';

/**
 * Application controller.
 * @constructor
 */
function App() {

    let chartView = new View();
    let chartModel = new Model();

    /**
     * Adds main search bar to document main element and designates
     * the needed identifications/classes to it.
     * Adds the complete search bar to View.SearchHandler
     */
    this.init = () => {

        let main = document.getElementById('main');
        let logo = document.getElementById('logo');
        let sun = document.getElementById('sun');

        let mainSearch = new chartModel.SearchElement(addNewChart);
        mainSearch.wrap.id = 'main-search';
        mainSearch.wrap.classList.add('search-wrap');

        chartView.add(mainSearch);
        main.appendChild(mainSearch.wrap);
        main.classList.remove('main-hidden');
        logo.classList.remove('logo-hidden');
        sun.classList.remove('sun-hidden');
    };

    /**
     * Forms a new chart and adds compare search bar to it,
     * changes the view to show the chart,
     * adds perCapita button functionality,
     * adds chart to View.ChartHandler,
     *
     * Propose to change this in the next revision, too complicated / extensive.
     *
     * @param response countryName: name, countryID: id, data: responseData,chartLabels: labels
     *
     */
    function addNewChart(response) {
        // Clear search results
        chartView.clearSearch();
        // Change view
        chartView.dataView();
        // Add new chart
        let chart = new chartModel.ChartElement();
        // Init the compare search
        let search = new chartModel.SearchElement(addNewSearchResult);
        search.getSearchBar().classList.add('compare-search');
        // Add compare search result to chart instead of creating new chart.
        function addNewSearchResult(response) {
            chartView.nextChartStyle();
            chart.parseData(response).setDataSetStyle(chartView.getDisplayConfig());
            chart.update();
            search.clear();
        }
        // Add to view
        chartView.add(chart);
        // Cycle colors
        chartView.nextChartStyle();
        // Adds search element as compare search to chart
        chart.setCompare(search);
        // Add to ChartHandler
        chart.parseData(response).setDataSetStyle(chartView.getDisplayConfig());
        chart.update();
    }
}

/**
 * Application model
 * @constructor
 */
function Model() {

    // Types used to identify element type.
    const types = {
        1: 'search',
        2: 'chart',
    };

    /**
     * Fetches data from server.
     * @param url
     * @returns {Promise<*>}
     */
    async function get(url) {
        return new Promise((resolve, reject) => {
            let xhttp = new XMLHttpRequest();
            xhttp.open('GET', url);
            xhttp.onload = function()
            {
                if(this.status === 200){
                    resolve(xhttp.response);
                } else{
                    reject(this.statusText);
                }
            };
            xhttp.onerror = function()
            {
                reject({status: this.status, text: this.statusText});
            };
            xhttp.send();
        })
    }

    /**
     * Gets search results from the server.
     * @param query search query
     * @returns {Promise<*>} A promise that returns the fetched data from server.
     */
    async function getSearchData(query) {
        if(!query) return;
        const url = `/api/country/search?q=${query}`;
        return get(url)
    }

    /**
     * Gets country data from the server and formats it.
     * @param callback Function to be called after the data has been fetched
     * @param name Country name
     * @param id Country three letter ID
     */
    async function getCountryData(callback, name, id) {

        const emissionUrl = `/api/country/${id}/co2emissions`;
        const populationUrl = `/api/country/${id}/population?getDates=true`;
        const wikiUrl = `/api/country/${name}/wiki`;

        const formLabels = (obj) => {
            if (obj.data[0].year) {
                return obj.data.map((datapoint) => {
                    return datapoint.year;
                }).reverse();
            }
        };

        Promise.all([get(emissionUrl), get(populationUrl), get(wikiUrl)])
            .then((data) => {

                let emission = JSON.parse(data[0]);
                let population = JSON.parse(data[1]);
                let wikiSummary = JSON.parse(data[2]);

                let labels = formLabels(population);

                population.data = population.data.map((datapoint) => {
                    return datapoint.value
                }).reverse();

                emission.data = emission.data.map((datapoint) => {
                    return datapoint.value;
                }).reverse();

                let responseData = [emission, population, wikiSummary];

                callback(
                    {
                        countryName: name,
                        countryID: id,
                        data: responseData,
                        chartLabels: labels,
                    });
            })
            .catch(err => console.log(err));
    }

    /**
     * SearchElement holds the information and required elements to form a functional
     * search bar that fetches data and displays them in result element.
     * @param callback A function to be invoked when a search result is clicked
     * @constructor
     */
    this.SearchElement = function(callback) {

        this.type = types["1"];

        const elements = {
            searchBar: document.createElement('input'),
            results: document.createElement('ul'),
        };

        // Stores all the SearchResults
        let searchResults = [];

        // Defines the max results to be displayed
        let searchResultMax = 8;

        elements.searchBar.placeholder = 'Type country name here...';
        elements.searchBar.addEventListener('keyup', () => {
            // Clear the search bar
            this.clear();

            // Get data from server
            getSearchData(elements.searchBar.value)
                .then((data) => {

                    // Parse the data and form new SearchResults
                    JSON.parse(data).forEach((searchResultData) => {
                        elements.searchBar.innerText = '';
                        this.add(new SearchResult(searchResultData,callback))
                    });

                    // Display the results
                    this.update();
                })
        });

        // Holds all elements related to search
        // this will be added to document for display
        this.wrap = document.createElement('div');
        this.wrap.classList.add('search-wrap');
        this.wrap.appendChild(elements.searchBar);
        this.wrap.appendChild(elements.results);

        /**
         * Add a new Searchresult to this
         * @param SearchResult
         */
        this.add = (SearchResult) => {
            searchResults.push(SearchResult);
        };

        /**
         * Updates result element with current search results
         */
        this.update = () => {
            elements.results.innerHTML = '';
            for(let i = 0; i < searchResultMax && i < searchResults.length; i++){
                elements.results.appendChild(searchResults[i].getWrap())
            }
        };

        /**
         * Empty results and result element
         */
        this.clear = () => {
            searchResults = [];
            elements.searchBar.innerText = '';
            elements.results.innerHTML = '';
        };

        /**
         * @returns {HTMLElement} Search bar
         */
        this.getSearchBar = () => {
            return elements.searchBar;
        };

        /**
         * SearchResult has the information and required elements to form a
         * functional search result.
         * @param searchResultData Parsed search result data
         * @param callback To be invoked when this (result) is clicked
         * @constructor
         */
        function SearchResult(searchResultData, callback) {
            let name = searchResultData[0];
            let id = searchResultData[1];
            let iso2code = searchResultData[2];

            let flag = document.createElement('img');
            flag.src = `https://www.countryflags.io/${iso2code}/shiny/32.png`;

            // Element for the search result
            let wrap = document.createElement('div');
            wrap.classList.add('result-wrap');
            wrap.appendChild(flag);
            wrap.innerHTML += `${name}<span>${id}</span>`;

            // Element to be added to document
            let listItem = document.createElement('li');
            listItem.addEventListener('click', () => {
                getCountryData(callback, name, id);
            });
            listItem.appendChild(wrap);

            this.getWrap = () => {
                return listItem;
            }
        }
    };

    /**
     * ChartElement represents the element that is displayed as a graph for
     * the user. Hold required UI elements, such as buttons and search fields.
     * @constructor
     */
    this.ChartElement = function () {

        const Config = new Configuration();

        // The chart element
        let chart = null;

        // Labels are the data on X-axis
        let labels = [];

        let compare = false;
        let showPerCap = false;

        // Holds all countries displayed in chart as CountryData.
        let countries = [];

        // Cursors determinate the scale and range of the displayed data.
        let cursorMin = 0;
        let cursorMax = 0;
        let scale = 10;

        this.type = types["2"];

        // Wrapping for chart.
        this.wrap = document.createElement('div');
        this.wrap.classList.add('chart');

        // Wiki summary
        this.wiki = document.createElement('div');
        this.wrap.appendChild(this.wiki);

        // Wrap for all the buttons
        this.buttonWrap = document.createElement('div');
        this.buttonWrap.classList.add('buttons');

        // Scroll data to left
        this.goBack = document.createElement('button');
        this.goBack.innerText = '<';
        this.buttonWrap.appendChild(this.goBack);

        // Scroll data to right
        this.goNext = document.createElement('button');
        this.goNext.innerText = '>';
        this.buttonWrap.appendChild(this.goNext);

        // Increase amount of data displayed in chart
        this.scaleUp = document.createElement('button');
        this.scaleUp.innerText = '+';
        this.buttonWrap.appendChild(this.scaleUp);

        // Decrease amount of data displayed in chart
        this.scaleDown = document.createElement('button');
        this.scaleDown.innerText = '-';
        this.buttonWrap.appendChild(this.scaleDown);

        // Toggle the per capita view
        this.perCapita = document.createElement('button');
        this.perCapita.innerText = 'Per Capita';
        this.buttonWrap.appendChild(this.perCapita);

        // Toggle the compare search bar
        this.compare = document.createElement('button');
        this.compare.innerText = 'Compare';
        this.buttonWrap.appendChild(this.compare);

        // Add buttons to element wrapper
        this.wrap.appendChild(this.buttonWrap);

        // Compare search bar, hidden in default
        let compareSearch = document.createElement('div');
        compareSearch.classList.add('compare');
        compareSearch.classList.add('hidden');
        this.wrap.appendChild(compareSearch);

        // Canvas for drawing the chart
        this.canvas = document.createElement('canvas');
        let ctx = this.canvas.getContext('2d');

        // Wrap for the canvas so that ChartJS can easily resize the chart
        this.canvasWrap = document.createElement('div');
        this.canvasWrap.classList.add('chart-container');
        this.canvasWrap.appendChild(this.canvas);
        this.wrap.appendChild(this.canvasWrap);

        /**
         * Clears and redraws the data on the chart. Data is reformed in the
         * process and the old config is overwritten. If no chart can be
         * found, creates a new one.
         *
         * @param showPerCap Determines whether chart displays per capita
         * or population and emission data
         */
        function update(showPerCap) {
            Config.empty();

            // Add countries dataset to config
            countries.forEach((country) => {
                country.formDataSetConfigurations(showPerCap).forEach((dataSet) => {
                    Config.addDatasetCfg(dataSet);
                })
            });

            Config.setLabels(labels.slice(cursorMin, cursorMax));

            // If no chart, create a new one. Update the chart dataset with
            // current configured dataset.
            if(!chart){
                chart = new Chart(ctx, Config.get());

            } else {
                chart.update(Config.get());
            }
        }
        this.update = (showPerCap) => {
            update(showPerCap);
        };

        /**
         * @returns {number} Length of the countries array
         */
        this.getCountryDataLength = () => {
            return countries.length;
        };

        /**
         * Sets the compare search element.
         * @param searchElement SearchElement for comparing.
         */
        this.setCompare = (searchElement) => {
            compareSearch.appendChild(searchElement.wrap);
        };

        /**
         * Sets the labels array to the given one.
         * @param labelData Array of labels to be displayed in the X-axis.
         */
        function setLabels(labelData) {
            labels = labelData;
        }

        /**
         * Resets the cursor to its initial position.
         */
        function resetCursor() {
            cursorMin = labels.length - scale;
            cursorMax = labels.length;
        }

        /**
         * Parses and forms a new CountryData from the response data. Adds
         * wiki summary to ChartElement.
         * @param responseData Formated response that contains co2,
         * population and wiki data
         *
         * @returns {CountryData} New completed CountryData
         */
        this.parseData = (responseData) => {

            // Forms per capita data from co2 and population data
            const createPerCap = (co2, pop) => {
                let perCapitaData = [];

                pop.data.forEach((popDataPoint, index) => {
                    if(co2.data[index]){
                        perCapitaData.push(Math.round(popDataPoint / co2.data[index]));
                    } else {
                        perCapitaData.push(null);
                    }
                });
                return {label: 'perCap', data: perCapitaData};
            };

            let country = new CountryData(responseData.countryName);

            // Add co2, population and wiki data to CountryData
            country.addAll(responseData.data);
            // Add per capita data to CountryData
            country.add(createPerCap(responseData.data[0], responseData.data[1]));

            // Add wiki summary to ChartElement
            this.wiki.appendChild(country.wiki);

            // Add CountryData to countries collection
            countries.push(country);

            // If labels are not set, sets ChartElement labels.
            if(labels.length === 0){
                setLabels(responseData.chartLabels);
            }
            // Reset cursor position
            resetCursor();

            // Add correct range of labels to oonfig
            Config.setLabels(labels.slice(cursorMin, cursorMax));

            return country;
        };

        // Event listeners

        // Compare search
        this.compare.addEventListener('click', function () {
            compare = !compare;
            if(compare) {
                compareSearch.classList.remove('hidden');
            } else {
                compareSearch.classList.add('hidden');
            }
        });

        // Per capita
        this.perCapita.addEventListener('click', function () {
            showPerCap = !showPerCap;
            update(showPerCap);
        });

        // Next button
        this.goNext.addEventListener('click', function () {
            if(!((cursorMax + scale) > labels.length)){
                cursorMax += scale;
                cursorMin += scale;
                update(showPerCap);
            }
        });

        // Back button
        this.goBack.addEventListener('click', function () {
            if(!((cursorMin - scale) < 0)) {
                cursorMax -= scale;
                cursorMin -= scale;
                update(showPerCap);
            }
        });

        // Scale down button
        this.scaleDown.addEventListener('click', function () {
            if(!((cursorMin + scale) >= cursorMax)){
                cursorMin += scale;
            }
            update(showPerCap);
        });

        // Scale up button
        this.scaleUp.addEventListener('click', function () {
            if(!((cursorMin - scale) < 0)){
                cursorMin -= scale;
            }
            update(showPerCap);
        });

        /**
         * Holds the default configuration for the chart. Used for adding,
         * resetting and clearing the datasets.
         * @constructor
         */
        function Configuration() {

            const defaulConfig = {
                type: 'bar',
                data: {
                    labels: [],
                    datasets: [],
                },
                options: {
                    layout: {
                      padding: {
                          left: 10,
                          right: 10,
                          top: 0,
                          bottom: 0,
                      }
                    },
                    responsive: true,
                    legend: {
                        labels: {
                            usePointStyle: true
                        },
                        boxWidth: 20,
                        display: true,
                        position: 'bottom'
                    },
                    scales: {
                        yAxes:[
                            {
                                id: 'A',
                                position: 'left',
                                type: 'linear',
                                ticks: {
                                    beginAtZero: false,
                                    fontColor: 'rgb(255,255,255)',
                                    callback: function(value, index, values) {
                                        return value.toLocaleString();
                                    }
                                },
                                scaleLabel: {
                                    fontColor: 'rgb(255,255,255)',
                                    display: true,
                                    labelString: 'Population'
                                },
                            },

                            {
                                id: 'B',
                                position: 'right',
                                gridLines: false,
                                type: 'linear',
                                ticks: {
                                    beginAtZero: true,
                                    fontColor: 'rgb(255,255,255)',
                                    callback: function(value, index, values) {
                                        return value.toLocaleString();
                                    }
                                },
                                scaleLabel: {
                                    fontColor: 'rgb(255,255,255)',
                                    display: true,
                                    labelString: 'Co2 Emissions'
                                },
                            },
                            ],
                        xAxes: [{
                            ticks: {
                                fontColor: 'rgb(255,255,255)',
                            }
                        }]
                    }
                }
            };

            let configuration = defaulConfig;

            /**
             * Get current configuration
             * @returns {Object} Current configuration
             */
            this.get = () => {
                return configuration;
            };

            /**
             * Reset configuration to default
             */
            this.reset = () => {
                configuration = defaulConfig;
            };

            /**
             * Empty the dataset array
             */
            this.empty = () => {
                configuration.data.datasets = [];
            };

            /**
             * Add dataset configuration to datasets
             * @param datasetCfg Configuration to be added
             */
            this.addDatasetCfg = (datasetCfg) => {
                configuration.data.datasets.push(datasetCfg);
            };

            /**
             * Set labels to configuration
             * @param labels Labels to be added
             */
            this.setLabels = (labels)  => {
                configuration.data.labels = labels;
            }
        }

        /**
         * Collection of country specific co2, population and per capita data.
         *
         * @param countryName Name of the country
         * @constructor
         */
        function CountryData(countryName) {

            let name = countryName;

            // co2, population and per capita data.
            let dataSets = [];

            /**
             * Forms an array of country specific datasets sliced to cursor
             * position.
             *
             * @param showPerCap Determines if per capita data is shown.
             * @returns {Array} Array of formatted dataset configurations.
             */
            this.formDataSetConfigurations = (showPerCap) => {
                let sets = [];
                dataSets.forEach((dataSet) => {
                    if(showPerCap && dataSet.labelType === 'perCap') {
                        sets.push(dataSet.slice(cursorMin, cursorMax));
                    } else if(!showPerCap && !(dataSet.labelType === 'perCap')) {
                        sets.push(dataSet.slice(cursorMin, cursorMax));
                    }
                });
                return sets;
            };

            /**
             * Sets a specific style to all the datasets this country has.
             *
             * @param styleCfg Style configuration to be added.
             */
            this.setDataSetStyle = (styleCfg) => {
                dataSets.forEach((dataSet) => {
                    dataSet.cfg = Object.assign(dataSet.cfg, styleCfg);
                });
            };

            /**
             * Creates a single WikiData or DataSet depending on the object
             * label.
             * @param obj Object that holds the data
             */
            this.add = (obj) => {
                if(obj.label === 'wiki') {
                    this.wiki = new WikiData(obj, name);
                } else {
                    new DataSet(obj, name);
                }
            };

            /**
             * For each object Creates WikiData or DataSet depending on the
             * object label
             * @param data Array of Objects that hold the data.
             */
            this.addAll = (data) => {
                data.forEach((obj) => {
                    if(obj.label === 'wiki') {
                        this.wiki = new WikiData(obj, name);
                    } else {
                        new DataSet(obj, name);
                    }
                })
            };

            /**
             * Creates a wiki element with images, heading and summary text
             *
             * @param wikiData Contains wikipedia summary and flag url
             * @param name Name of the country
             * @returns {HTMLElement} Completed wiki element
             * @constructor
             */
            function WikiData(wikiData, name){
                let wiki = document.createElement('div');
                wiki.classList.add('wiki');

                let imgContainer = document.createElement('div');
                imgContainer.classList.add('image-container');

                let img = document.createElement('img');
                img.src = wikiData.data.flag;
                img.classList.add('flag');
                imgContainer.appendChild(img);

                let summary = document.createElement('div');
                let heading = document.createElement('h1');
                heading.innerText = name;
                summary.appendChild(heading);

                let summaryText = document.createElement('p');
                summaryText.innerText = wikiData.data.summary;
                summary.appendChild(summaryText);

                summary.classList.add('wikitext');

                wiki.appendChild(imgContainer);
                wiki.appendChild(summary);
                return wiki;
            }

            /**
             * Creates a DataSet with correct configuration and functionality
             * depending on data.label
             * @param data Dataset data
             * @constructor
             */
            function DataSet(data, name) {

                const names =  {
                    'co2': " Co2 emissions",
                    'pop': " Population",
                    'perCap': " Co2 emissions per capita",
                };

                const types = {
                    'co2': "line",
                    'pop': "bar",
                    'perCap': "line",
                };

                const axisID = {
                    'co2': "B",
                    'pop': "A",
                    'perCap': "A",
                };

                const pointStyle = {
                    'co2': "circle",
                    'pop': "rect",
                    'perCap': "circle",
                };

                const countryData = data.data;

                this.labelType = data.label;

                this.cfg = {
                    type: types[data.label],
                    yAxisID: axisID[data.label],
                    label: name + names[data.label],
                    dataType: data.label,
                    data: data.data,
                    pointStyle: pointStyle[data.label],
                };

                /**
                 * Creates a new DataSet configuration from the original data.
                 * @param begin Begin index
                 * @param end End index
                 * @returns {{type: *, yAxisID: *, label: *, data: *}|*}
                 */
                this.slice = (begin, end) => {
                    this.cfg.data = countryData.slice(begin, end);
                    return this.cfg;
                };

                dataSets.push(this);
            }
        }
    };

}

/**
 * Application view
 * @constructor
 */
function View() {

    // Big logo element
    const logoWrap = document.getElementById('logo-wrap');

    const colors = [
        'rgb(20, 100, 60)',
        'rgb(255, 20, 60)',
        'rgb(0, 135, 200)',
        'rgb(255,255,255)'];

    const dataView = {
        element: {
            header: document.getElementById('header'),
        },
    };

    let mainSearch = null;

    /**
     * Cycles through colors and sets default display config border color.
     */
    this.nextChartStyle = () =>  {
        ChartHandler.displaycfg.borderColor =
            colors[ChartHandler.chart.getCountryDataLength()];
    };

    /**
     * @returns {Object} default display configuration
     */
    this.getDisplayConfig = () => {
        return ChartHandler.displaycfg;
    };

    /**
     * Adds an element to view.
     * @param element Element to be added.
     */
    this.add = (element) => {
        const types = {
            'chart': (chart) => {
                ChartHandler.chart = chart;
                ChartHandler.addChart(chart);
            },
            'search': (SearchElement) => {
                mainSearch = SearchElement;
            }
        };
        types[element.type](element);
    };

    /**
     * Clears the main search.
     */
    this.clearSearch = () => {
        mainSearch.clear();
    };

    /**
     * Change document view to show the chart.
     */
    this.dataView = () => {
        mainSearch.wrap.classList.add('top');
        logoWrap.classList.add('hidden');
        dataView.element.header.appendChild(mainSearch.wrap);
        dataView.element.header.classList.remove('hidden');
    };



    const ChartHandler = {

        chart: null,

        //DataSet
        displaycfg: {
            backgroundColor: 'rgb(44, 44, 44)', // inside
            borderColor: 'rgb(255,0,0)', // outline
            borderWidth: 3,
            pointRadius: 20,
            fill: false,
        },

        element: {
            chartWrap: document.getElementById('chart'),
        },

        addChart: (chart) => {
            ChartHandler.element.chartWrap.innerHTML = '';
            ChartHandler.element.chartWrap.appendChild(chart.wrap);
        },
    };
}

window.onload = function() {
    const Application = new App();
    Application.init();
};
